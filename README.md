# JsonParser

@[TOC](Gson和FastJson比较)

首先说说Gson和Fastjson的相关使用，详细博文 [链接点我](https://blog.csdn.net/CSqingchen/article/details/48803267)
 
 都说FastJson更快，为什么那么快呢，已有许多博文解析，比如 [链接点我](https://blog.csdn.net/u012961566/article/details/76944982)

#### 测试代码
全部 [代码地址](https://gitee.com/chenjimcom/JsonParser)
```
                Student student = initStudent();
                String json = "";
                long time0;

                time0 = SystemClock.elapsedRealtime();
                for (int i = 0; i < FOR_NUM; i++) {
//                    gson = new Gson();
                    json = gson.toJson(student);
                }
                Log.d(TAG, "Gson Object to json time:" + (SystemClock.elapsedRealtime() - time0));
                //Log.d(TAG, "Gson Object to json :" + json);

                time0 = SystemClock.elapsedRealtime();
                for (int i = 0; i < FOR_NUM; i++) {
//                    gson = new Gson();
                    student = gson.fromJson(json, Student.class);
                }
                Log.d(TAG, "Gson json to Object time:" + (SystemClock.elapsedRealtime() - time0));

                time0 = SystemClock.elapsedRealtime();
                for (int i = 0; i < FOR_NUM; i++) {
                    json = JSON.toJSONString(student);
                }
                Log.d(TAG, "JSON Object to json time:" + (SystemClock.elapsedRealtime() - time0));
                //                Log.d(TAG, "JSON Object to json :" + json);

                time0 = SystemClock.elapsedRealtime();
                for (int i = 0; i < FOR_NUM; i++) {
                    student = JSON.parseObject(json, Student.class);
                }
                Log.d(TAG, "JSON json to Object time:" + (SystemClock.elapsedRealtime() - time0));

                //Log.d(TAG, "result:" + JSON.toJSONString(student));
```
#### 测试结果
```
05-06 14:05:28.261 19015 19126 D GsonFastjson: Gson Object to json time:225
05-06 14:05:28.473 19015 19126 D GsonFastjson: Gson json to Object time:211
05-06 14:05:28.648 19015 19126 D GsonFastjson: JSON Object to json time:176
05-06 14:05:28.927 19015 19126 D GsonFastjson: JSON json to Object time:279
```
#### 测试结论
- FastJson将对象转json字符串更快一些
- FastJson将json转对象稍微慢一些