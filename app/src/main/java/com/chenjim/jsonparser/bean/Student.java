package com.chenjim.jsonparser.bean;

import java.util.List;
import java.util.Set;

/**
 * Created by jim.chen on 2019/5/5.
 */
public class Student {
    private String name;
    private int age;
    private String sex;
    private String describe;
    private Set<Book> bookSet;

    private List<Book> bookList;

    public Student() {
    }

    public Student(String name, int age, String sex, String describe) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.describe = describe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public Set<Book> getBookSet() {
        return bookSet;
    }

    public void setBookSet(Set<Book> bookSet) {
        this.bookSet = bookSet;
    }

    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }
}
