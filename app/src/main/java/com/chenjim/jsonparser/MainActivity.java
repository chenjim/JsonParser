package com.chenjim.jsonparser;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.alibaba.fastjson.JSON;
import com.chenjim.jsonparser.bean.Book;
import com.chenjim.jsonparser.bean.Student;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "GsonFastjson";
    private static final int FOR_NUM = 1000;

    private static ExecutorService executorService = Executors.newCachedThreadPool();

    private Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonAction();
            }
        });

    }

    private void buttonAction() {

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                Student student = initStudent();
                String json = "";
                long time0;

                time0 = SystemClock.elapsedRealtime();
                for (int i = 0; i < FOR_NUM; i++) {
//                    gson = new Gson();
                    json = gson.toJson(student);
                }
                Log.d(TAG, "Gson Object to json time:" + (SystemClock.elapsedRealtime() - time0));
                //Log.d(TAG, "Gson Object to json :" + json);

                time0 = SystemClock.elapsedRealtime();
                for (int i = 0; i < FOR_NUM; i++) {
//                    gson = new Gson();
                    student = gson.fromJson(json, Student.class);
                }
                Log.d(TAG, "Gson json to Object time:" + (SystemClock.elapsedRealtime() - time0));

                time0 = SystemClock.elapsedRealtime();
                for (int i = 0; i < FOR_NUM; i++) {
                    json = JSON.toJSONString(student);
                }
                Log.d(TAG, "JSON Object to json time:" + (SystemClock.elapsedRealtime() - time0));
                //                Log.d(TAG, "JSON Object to json :" + json);

                time0 = SystemClock.elapsedRealtime();
                for (int i = 0; i < FOR_NUM; i++) {
                    student = JSON.parseObject(json, Student.class);
                }
                Log.d(TAG, "JSON json to Object time:" + (SystemClock.elapsedRealtime() - time0));

                //Log.d(TAG, "result:" + JSON.toJSONString(student));
            }
        });

    }

    private Student initStudent() {
        Set<Book> bookSet = new HashSet<>();
        bookSet.add(new Book("001", "N001", 1.00F));
        bookSet.add(new Book("002", "N002", 2.00F));
        bookSet.add(new Book("003", "N003", 3.00F));

        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("001", "N001", 1.00F));
        bookList.add(new Book("002", "N002", 2.00F));
        bookList.add(new Book("003", "N003", 3.00F));

        Student student = new Student("123", 20, "boy", "123D");
        student.setBookList(bookList);
        student.setBookSet(bookSet);

        return student;
    }


}
